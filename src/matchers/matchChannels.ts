import { Matcher } from ".."
import { TextChannel } from "discord.js"

/**
 * Matches if the message channel satisfies any of the strings passed to the matcher
 *
 * Supported types:
 * - IDs - "334283194146881536"
 * - Names - "general"
 */
export const matchChannels = (...channels: string[]): Matcher => context => {
  const { channel } = context.message

  const hasMatch = channels.some(c => {
    if (/^[0-9]+$/.test(c)) {
      return channel.id === c
    }

    if (channel instanceof TextChannel) {
      return channel.name.toLowerCase() === c.toLowerCase()
    }

    return false
  })

  if (hasMatch) return context
}
