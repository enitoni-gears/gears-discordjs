import { Matcher } from ".."
import { TextChannel } from "discord.js"

/**
 * Matches if the channel is deemed not safe for work
 */
export const matchNSFW = (): Matcher => context => {
  const { channel } = context.message
  const matches = channel instanceof TextChannel && channel.nsfw
  if (matches) return context
}
