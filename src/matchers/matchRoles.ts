import { Matcher } from ".."

/**
 * Matches if the message member's roles satisfies any of the strings passed to the matcher
 *
 * Supported types:
 * - IDs - "335481043077300225"
 * - Names - "Wavey"
 */
export const matchRoles = (...roles: string[]): Matcher => async (context) => {
  const { message } = context
  const { member } = message

  if (!member) return

  const { cache: memberRoles } = member.roles

  const hasMatch = roles.some((role) => {
    if (/^[0-9]+$/.test(role)) {
      return memberRoles.has(role)
    }

    return !!memberRoles.find((r) => r.name.toLowerCase() === role.toLowerCase())
  })

  if (hasMatch) return context
}
