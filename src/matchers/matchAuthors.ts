import { Matcher } from ".."

/**
 * Matches if the message author satisfies any of the strings passed to the matcher.
 *
 * Supported types:
 * - IDs - "185820463220391937"
 * - Usernames - "Enitoni"
 * - Usernames & discriminators - "Enitoni#0001"
 */
export const matchAuthors = (...users: string[]): Matcher => context => {
  const { author } = context.message

  const hasMatch = users.some(user => {
    if (/^[0-9]+$/.test(user)) {
      return author.id === user
    }

    const [username, discriminator] = user.split("#")

    if (discriminator) {
      return author.discriminator === discriminator && author.username === username
    }

    return author.username === username
  })

  if (hasMatch) return context
}
