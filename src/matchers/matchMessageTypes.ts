import { MessageType } from "../types"
import { Matcher } from ".."

/**
 * Matches if message type is one of the types passed to this matcher
 */
export const matchMessageTypes = (...types: MessageType[]): Matcher => context => {
  const { message } = context
  const type = message.type as MessageType

  if (types.includes(type)) return context
}
