import { core } from "@enitoni/gears"
import { Client, ClientOptions, Message } from "discord.js"

export interface AdapterOptions extends ClientOptions {
  token: string
  listenToSelf?: boolean
}

export class Adapter extends core.ClientAdapter<Message, Client, AdapterOptions> {
  protected register(options: AdapterOptions, hooks: core.AdapterHooks<Message>) {
    const { token, listenToSelf, ...clientOptions } = options
    const client = new Client(clientOptions)

    client.on("message", (message: Message) => {
      if (client.user?.id !== message.author.id || listenToSelf) {
        hooks.message(message)
      }
    })

    client.on("ready", hooks.ready)
    client.on("resume", hooks.ready)
    client.on("disconnect", hooks.unready)
    client.on("error", hooks.error)

    return {
      client,
      methods: {
        start: async () => {
          await client.login(token)
        },
        getMessageContent: (message: Message) => message.content,
      },
    }
  }
}

export type Context<S extends object = {}> = core.Context<S, Message, Client>
export type Matcher<S extends object = {}> = core.Matcher<S, Message, Client>
export type Middleware<S extends object = {}> = core.Middleware<S, Message, Client>

export declare class Command<D = unknown> extends core.Command<Message, Client, D> {}

export declare class CommandGroup<D = unknown> extends core.CommandGroup<
  Message,
  Client,
  D
> {}

export declare class Service extends core.Service<Message, Client> {}
export declare class Bot extends core.Bot<Message, Client> {}

exports.Command = core.Command
exports.CommandGroup = core.CommandGroup
exports.Service = core.Service
exports.Bot = core.Bot

export * from "./matchers"
export * from "./services"
