import { Service } from ".."
import chalk from "chalk"

type LogType = "success" | "info" | "error" | "warning"

const colorMap: Record<LogType, (x: string) => string> = {
  success: chalk.green,
  info: chalk.cyan,
  error: chalk.red,
  warning: chalk.yellow
}

/** Logs general information, warnings, and errors to the console */
export class ConsoleLoggingService extends Service {
  protected async serviceDidInitialize() {
    const { log, bot } = this
    const { client } = bot

    client.on("ready", () => log("Connected"))
    client.on("resume", () => log("Reconnected", "success"))
    client.on("reconnecting", () => log("Reconnecting...", "warning"))
    client.on("disconnect", () => log("Disconnected", "error"))
    client.on("rateLimit", () => log("Bot is being rate limited", "warning"))
    client.on("warn", warning => log(warning, "warning"))
    client.on("error", error => log(error.toString(), "error"))

    bot.on("error", error => log(error.toString(), "warning"))
  }

  private log(message: string, type: LogType = "info") {
    const date = new Date()
    const coloredDate = chalk.grey(
      `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
    )

    const coloredMessage = colorMap[type](message)
    console.log(`${coloredDate} - ${coloredMessage}`)
  }
}
